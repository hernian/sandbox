// usjp2.c

#include <stdint.h>
#include "quantum.h"


typedef struct {
    uint16_t    keycode_in;
    uint8_t     keycode_out;
    uint8_t     index_flag:6;
    bool        with_shift_in:1;
    bool        with_shift_out:1;
} usjp_conv_t;


#define USJP_CONV_COUNT 8

#define COUNT_ELEMENTS(tbl) (sizeof(tbl) / sizeof((tbl)[0]))

enum jp_keycodes {
    KCJP_ATMARK = 0x2f
};

static const usjp_conv_t USJP_CONV_TABLE[] = {
    {  0, KC_2, KCJP_ATMARK, true, false }
};
#define USJP_COUNT_CONV_TABLE   COUNT_ELEMENTS(USJP_CONV_TABLE)


static const uint8_t USJP_MOD_BIT_LSHIFT = MOD_BIT(KC_LSHIFT);
static const uint8_t USJP_MOD_BIT_RSHIFT = MOD_BIT(KC_RSHIFT);

static uint8_t usjp_real_mods;
static uint8_t usjp_count_active;
static uint8_t usjp_conv_flags[(USJP_COUNT_CONV_TABLE + 7) / 8];
static const usjp_conv_t* usjp_conv_last;


static inline bool usjp_is_shift_mods(uint8_t mods){
    return (mods & 0x11) != 0;
}


static inline bool usjp_is_shift_key(uint16_t keycode){
    return (keycode == KC_LSHIFT) || (keycode == KC_RSHIFT);
}


static inline bool usjp_is_active(const usjp_conv_t* conv){
    uint8_t offset = conv->index_flag / 8;
    uint8_t mask = 1 << (conv->index_flag % 8);
    return (usjp_conv_flags[offset] & mask) != 0;
}


static void usjp_set_active(const usjp_conv_t* conv){
    uint8_t offset = conv->index_flag / 8;
    uint8_t mask = 1 << (conv->index_flag % 8);
    if ((usjp_conv_flags[offset] & mask) != 0){
        return;
    }
    usjp_conv_flags[offset] |= mask;
    ++usjp_count_active;
    usjp_conv_last = conv;
}


static void usjp_set_deactive(const usjp_conv_t* conv){
    uint8_t offset = conv->index_flag / 8;
    uint8_t mask = 1 << (conv->index_flag % 8);
    if ((usjp_conv_flags[offset] & mask) == 0){
        return;
    }
    usjp_conv_flags[offset] &= ~mask;
    --usjp_count_active;
    if (usjp_conv_last == conv){
        usjp_conv_last = NULL;
    }
}


static void usjp_get_pseuto_shift_keycode(){
    it ()
}

static const usjp_conv_t* usjp_find_conv(uint16_t keycode){
    for (uint16_t i = 0; i < USJP_COUNT_CONV_TABLE; ++i){
        const usjp_conv_t* conv = &USJP_CONV_TABLE[i];
        if (conv->keycode_in == keycode){
            return conv;
        }
    }
    return NULL;
}


static const usjp_set_pseudo_shift(const usjp_conv_t* conv){
    uint8_t real_mods_pre = get_mods();
    uint8_t real_mods_post = real_mods_pre;
    uint8_t weak_mods_pre = get_weak_mods();
    uint8_t weak_mods_post = weak_mods_pre;
    if (conv->with_shift_out){
        // 実際に押されているシフトキーを優先するが、左シフトも右シフトも押されていないときは右シフトが押されたことにする。
        if ((real_mods_pre & USJP_MOD_BIT_LSHIFT) != 0){
            real_mods_post |= USJP_MOD_BIT_LSHIFT;
        }
        if ((real_mods_pre & USJP_MOD_BIT_RSHIFT) != 0){
            real_mods_post |= USJP_MOD_BIT_RSHIFT;
        }
        if (((real_mods_pre & USJP_MOD_BIT_LSHIFT) == 0) && ((real_mods_pre & USJP_MOD_BIT_RSHIFT) == 0)){
            real_mods_post |= USJP_MOD_BIT_RSHIFT;
        }
        if ((weak_mods_pre & USJP_MOD_BIT_LSHIFT) != 0){
            weak_mods_post |= USJP_MOD_BIT_LSHIFT;
        }
        if ((weak_mods_pre & USJP_MOD_BIT_RSHIFT) != 0){
            weak_mods_post |= USJP_MOD_BIT_RSHIFT;
        }
        if (((weak_mods_pre & USJP_MOD_BIT_LSHIFT) == 0) && ((weak_mods_pre & USJP_MOD_BIT_RSHIFT) == 0)){
            weak_mods_post |= USJP_MOD_BIT_RSHIFT;
        }
    }
    else{
        real_mods_post &= ~USJP_MOD_BIT_LSHIFT;
        real_mods_post &= ~USJP_MOD_BIT_RSHIFT;
        weak_mods_post &= ~USJP_MOD_BIT_LSHIFT;
        weak_mods_post &= ~USJP_MOD_BIT_RSHIFT;
    }
    if ((real_mods_pre != real_mods_post) || (weak_mods_pre != weak_mods_post)){
        set_mods(real_mods_post);
        set_weak_mods(weak_mods_post);
        send_keyboard_report();
    }
}


static const usjp_sync_shift(){
    // TODO: 実装せよ
}


static bool usjp_convert(uint16_t keycode, keyrecord_t* record){
    if (usjp_is_shift_key(keycode){
        uint8_t mod_bit = MOD_BIT(keycode);
        if (record->event.pressed){
            usjp_real_mods |= mod_bit;
        }
        else{
            usjp_real_mods &= ~mod_bit;
        }
        return false;
    }
    const usjp_conv_t* conv = usjp_find_conv(keycode);
    if (conv != NULL){
        if (record->event.pressed){
            if (conv->with_shift_in != usjp_is_shift_mods(usjp_real_mods)){
                return false;
            }
            if (usjp_is_active(conv)){
                return false;
            }
            usjp_set_active(conv);
            usjp_set_pseudo_shift(conv->with_shift_out);
            register_code(conv->keycode_out);
        }
        else{
            if (usjp_is_active(conv) == false){
                return false;
            }
            unregister_code(conv->keycode_out);
            usjp_sync_shift();
            usjp_set_deactive(conv);
        }
        return true;
    }
    return false;
}


bool usjp_process_record(uint16_t keycode, keyrecord_t* record){
    // TODO: shiftキーの扱い

    if (conv != NULL){
        usjp_convert(conv, record);
        return false;
    }
    return true;
}
